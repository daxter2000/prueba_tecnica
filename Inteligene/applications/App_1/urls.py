from django.contrib import admin
from django.urls import path
from . import views

app_name = 'App1_app'
urlpatterns = [
    path('', views.EntradasListView.as_view(), name = 'inicio'),
    path('home/', views.EntradasListView.as_view(), name = 'inicio'),
    path('new_Entrada/', views.EntradaCreateView.as_view(), name = 'Nueva-Entrada'),
    path('editar/<pk>/', views.Editar_Entradas.as_view(), name = 'Editar-Entrada'),
    path('eliminar_entrada/<pk>/', views.EntradaDelete.as_view(), name = 'Eliminar-Entrada'),
    path('ver_entrada/<id_entrada>/', views.VerEntrada.as_view(), name = 'Ver-Entrada'),

]

#AQUI ESCRIBIREMOS LAS PERSONALIZACIONES DE LOS FORMLARIOS 

from django import forms
from .models import Comentario, Entrada


#FORMULARIO QUE PARTE DE UN MODELO HEREDANDO LOS ATRIBUTOS Y CARACTERISTICAS DE ESTE

class SetFormEntrada(forms.ModelForm):
    """Form definition for MODELNAME."""

    class Meta:
        """Meta definition for MODELNAMEform."""

        model = Entrada
        fields = ('autor','titulo','cuerpo')

        widgets ={
            'titulo': forms.TextInput(
                attrs = {
                    'placeholder': 'Titulo de la publicación',
                }
            )
            ,
            'autor': forms.TextInput(
                attrs = {
                    'placeholder': 'Autor',
                }
            ),
        }


class SetFormComentario(forms.ModelForm):
    """Form definition for MODELNAME."""

    class Meta:
        """Meta definition for MODELNAMEform."""

        model = Comentario
        fields = ('c_autor','c_cuerpo',)

        widgets ={

            'c_autor': forms.TextInput(
                attrs = {
                    'placeholder': 'Autor',
                }
            ),
        }

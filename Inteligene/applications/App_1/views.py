from django.db import models
from django.shortcuts import render
from django.urls import reverse_lazy
from itertools import chain
from django.views.generic import *
from .models import Entrada, Comentario
from .forms import SetFormEntrada, SetFormComentario


# Create your views here.


class EntradaCreateView(CreateView):
    model = Entrada
    template_name = "principal/crear_entrada.html"
    form_class = SetFormEntrada
    success_url= reverse_lazy('App1_app:inicio')

    def form_valid(self, form):

        form.save() #Con esto ejecutamos el registtro. se puede anular con 'commit = False'
        return super().form_valid(form)

class EntradasListView(ListView):
    model = Entrada
    template_name = "principal/home.html"
    context_object_name = 'entradas'


class Editar_Entradas(UpdateView):
    model = Entrada
    template_name = "principal/editar.html"
    fields=['autor','titulo','cuerpo']
    success_url= reverse_lazy('App1_app:inicio')

class EntradaDelete(DeleteView):
    model = Entrada
    template_name = "principal/delete.html"
    success_url= reverse_lazy('App1_app:inicio')
    

class VerEntrada(CreateView):
    template_name = "principal/ver_entrada.html"
    context_object_name = 'entrada'
    model = Comentario
    success_url= reverse_lazy('App1_app:inicio')
    form_class =SetFormComentario
    
    

    def form_valid(self, form):
        print('############## METODO FORM VALID###########################')
        id_entrada = self.kwargs['id_entrada']
        objeto_entrada = Entrada.objects.get(id=id_entrada)
        print(objeto_entrada)
        form.instance.id_Entrada = objeto_entrada
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context =  super().get_context_data(**kwargs)
        id_entrada = self.kwargs['id_entrada']
        objeto_entrada = Entrada.objects.get(id=id_entrada)
        comentarios= Comentario.objects.filter(id_Entrada=objeto_entrada)
        context['comentarios'] = comentarios
        context['entradas'] = objeto_entrada
        return context
    

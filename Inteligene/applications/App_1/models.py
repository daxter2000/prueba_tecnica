from django.db import models
#apps terceros
from model_utils.models  import TimeStampedModel
# Create your models here.

class Entrada(TimeStampedModel):
    autor = models.CharField('Autor', max_length=50)
    titulo = models.CharField('Titulo', max_length=50)
    cuerpo = models.TextField()

    class Meta:
        verbose_name= 'Entrada'
        verbose_name_plural= 'Entradas' ##DECORADORES
        ordering = ['-id'] #Agregar orden
        unique_together = ('autor', 'cuerpo') #No queremos que se registren dos parametros iguales en un registro


    def __str__(self):
        return str(self.id) + '-' + self.autor + '-' +self.titulo

class Comentario(TimeStampedModel):
    c_autor = models.CharField('Autor', max_length=50)
    c_cuerpo = models.TextField()
    id_Entrada = models.ForeignKey(Entrada, on_delete=models.CASCADE)

    class Meta:
        verbose_name= 'Comentario'
        verbose_name_plural= 'Comentarios' ##DECORADORES

    def __str__(self):
        return str(self.id) + '-' + str(self.id_Entrada) + ' '+ self.c_autor

class Subcomentario(TimeStampedModel):
    subc_autor = models.CharField('Autor', max_length=50)
    subcc_cuerpo = models.TextField()
    id_comment = models.ForeignKey(Comentario, on_delete=models.CASCADE)

    class Meta:
        verbose_name= 'Subcomentario'
        verbose_name_plural= 'Subcomentarios' ##DECORADORES

        
        def __str__(self):
            return str(self.id) 
